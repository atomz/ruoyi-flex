package com.ruoyi.mf.domain.bo;

import com.ruoyi.mf.domain.Customer;
import com.ruoyi.mf.domain.Goods;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.orm.core.domain.BaseEntity;

/**
 * 客户主表业务对象 mf_customer
 *
 * @author 数据小王子
 * @date 2024-01-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Customer.class, reverseConvertGenerate = false)
public class CustomerBo extends BaseEntity
{

    /**
     * 客户id
     */
    private Long customerId;

    /**
     * 客户姓名
     */
    @NotBlank(message = "客户姓名不能为空")
    private String customerName;

    /**
     * 手机号码
     */
    @NotBlank(message = "手机号码不能为空")
    private String phonenumber;

    /**
     * 客户性别
     */
    @NotBlank(message = "客户性别不能为空")
    private String gender;

    /**
     * 客户生日
     */
    @NotNull(message = "客户生日不能为空")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 客户描述
     */
    @NotBlank(message = "客户描述不能为空")
    private String remark;

    /** 商品子信息 */
    private List<Goods> goodsList;
}
